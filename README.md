# [greenlock-manager-fs.js](https://git.rootprojects.org/root/greenlock-manager-fs.js)

A simple file-based management strategy for Greenlock v3

(to manage SSL certificates for sites)

## Install

```js
npm install --save greenlock-manager-fs@v3
```

## Usage

```js
npx greenlock init --manager greenlock-manager-fs --config-dir './greenlock.d'
```

Or, place this file in the root of your project:

`.greenlockrc`:

```json
{
	"manager": { "module": "@greenlock/manager" },
	"configDir": "./greenlock.d"
}
```

## Example config file

You might start your config file like this:

`./greenlock.d/config.json`:

```json
{
	"subscriberEmail": "jon@example.com",
	"agreeToTerms": true,
	"sites": [
		{
			"subject": "example.com",
			"altnames": ["example.com", "*.example.com"]
		}
	]
}
```

## CLI Management (coming soon)

We're going to be adding some tools to greenlock so that you can do
something like this to manage your sites and SSL certificates:

```js
npx greenlock defaults --subscriber-email jon@example.com --agree-to-terms true
```

```js
npx greenlock add --subject example.com --altnames example.com,*.example.com
```

```js
npx greenlock renew --all
```
